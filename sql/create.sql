create table packages (
  -- not actually used; I assumed it would be the same as rowid
  id             decimal   unique primary key,
  devpkg         text      unique,
  virtual        boolean
);

create table dumps (
  id             decimal   unique primary key,
  status         text,
  check (
       status = 'failed'
    or status = 'uninstallable'
    or status = 'skipped'
    or status = 'dumped'
  )
);

create table analysis (
  id             decimal   unique primary key,
  analyzed       boolean,
  time_t         boolean,
  lfs            boolean,
  check (
    analyzed
    or (not analyzed and (not time_t and not lfs))
  )
);

create table quirks (
  id             decimal   unique primary key,
  extra_args         text,
  extra_deps         text,
  defines            text,
  includes           text,
  preamble           text,
  exclude_types      text,
  clippy_packages    text,
  installed_packages text
);

create table virtual_packages (
  id             decimal   unique primary key,
  real           text
);
